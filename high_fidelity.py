import numpy as np
from dolfin import *
from mshr import *
import progressbar

set_log_level(50)

class source_term(UserExpression):
	def set_source(self,xs):
		self.xs = xs
	def eval(self, values, x):
		values[0] = 10*exp(-(pow(x[0] - self.xs[0] , 2) + pow(x[1] - self.xs[1], 2)) / 0.1)

def boundary(x):
	return x[0] < -1 + DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS or x[1] < -1 + DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS

class high_fidelity():
	def __init__(self):
		self.mesh = Mesh('mesh.xml')
		self.V = FunctionSpace(self.mesh, 'CG', 1)

		self.u0 = Constant(0.0)
		self.bc = DirichletBC(self.V, self.u0, boundary)

		self.u = TrialFunction(self.V)
		self.v = TestFunction(self.V)

		self.dt = 0.05
		self.MAX_ITER = 40

		FEM_el = self.V.ufl_element()
		self.f = source_term(element = FEM_el)

	def set_initial_zero(self):
		self.u_n = interpolate(self.u0, self.V)

	def set_source_location(self, xs):
		self.f.set_source(xs)

	def assemble_variational_problem(self):
		F = self.u*self.v*dx + self.dt*dot(grad(self.u), grad(self.v))*dx - (self.u_n + self.dt*self.f)*self.v*dx
		self.a, self.L = lhs(F), rhs(F)

	def time_integration(self):
		u = Function(self.V)
		vtkfile = File('./solution/heat.pvd')

		t = 0
		time_snaps = []
		
		#for i in progressbar.progressbar(range(0,self.MAX_ITER)):
		for i in range(0,self.MAX_ITER):
			t += self.dt
			solve(self.a == self.L, u, self.bc)
			time_snaps.append( u.vector().get_local() )

			#vtkfile << (u,t)
			self.u_n.assign(u)

		return  np.reshape(time_snaps,[self.MAX_ITER,-1])

	def save_from_mat(self,mat,path='./solution/heat.pvd'):
		u = Function(self.V)
		vtkfile = File(path)

		t = 0
		for i in range(mat.shape[0]):
			vec = mat[i]
			u.vector().set_local(vec)
			vtkfile << (u,t)
			t += self.dt

def data_generation(num_data=500,file_name='raw_data.npz'):
	hf = high_fidelity()

	raw_data = []
	#for i in range(num_data):
	for i in progressbar.progressbar(range(num_data)):
		xs = np.random.uniform(-0.5,0.5,2)
		hf.set_initial_zero()
		hf.set_source_location(xs)
		hf.assemble_variational_problem()
		ts = hf.time_integration()
		raw_data.append(ts)

	raw_data = np.reshape(raw_data,[num_data,raw_data[0].shape[0],-1])
	np.savez(file_name,raw_data=raw_data)

if __name__ == '__main__':
	#hf = hight_fidelity()
	#hf.set_source_location(np.array([-0.25,0.25]) )
	#hf.assemble_variational_problem()
	#hf.time_integration()
	data_generation(num_data = 1, file_name='test_data.npz')

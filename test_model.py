import numpy as np
import torch
import high_fidelity as hf
import nn_network as neural_net

if __name__=='__main__':
	nn = neural_net.network_eval('./model.pt')
	nn_eval = nn.eval_model

	data = np.load('test_data.npz')
	train_data = data['raw_data'][0]

	in_list = []
	result = []
	for i in range(3):
		in_list.append(train_data[i])
		result.append(train_data[i])


	in_vec = np.reshape( in_list, [-1] )

	for i in range(3,40):
		torch_in = torch.from_numpy( np.reshape(in_vec,[1,-1]) ).float()
		#torch_in = torch.from_numpy( np.reshape(train_data[i-3:i],[1,-1]) ).float()
		out_vec = nn_eval(torch_in)

		in_list.pop(0)
		in_list.append(out_vec)
		result.append(out_vec)
		in_vec = np.reshape( in_list, [-1] )

	result = np.reshape(result,[len(result),-1])

	high_fid = hf.high_fidelity()
	high_fid.save_from_mat(result,'./solution/heat.pvd')
	high_fid.save_from_mat(train_data,'./solution/heat2.pvd')

#	high_fid = hf.high_fidelity()
#	noise = np.random.normal(0,0.003,train_data.shape)
#	high_fid.save_from_mat(train_data+noise,'./solution/heat2.pvd')	

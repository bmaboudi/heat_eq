import numpy as np
import high_fidelity as hf
import torch
import progressbar

class data():
	def __init__(self,num_batches=0,batch_size=0,path=None):
		if(path == None):
			self.num_batches = num_batches
			self.batch_size = batch_size
			self.num_data = self.batch_size*self.num_batches

			self.in_data = list()
			self.out_data = list()

			self.torch_in = list()
			self.torch_out = list()

			self.high_fid = hf.high_fidelity()

		else:
			self.batch_size = batch_size

			data = np.load(path)
			self.raw_in = data['raw_in']
			self.raw_out = data['raw_out']
			num_data = self.raw_in.shape[0]
			self.num_batches = int(num_data/self.batch_size)
			self.num_data = self.num_batches*self.batch_size
			self.torch_in = list()
			self.torch_out = list()

			self.shuffle()


	def generate_raw(self,num_raw_data=500,path='raw_data.npz'):
		raw_data = []
		#for i in range(num_data):
		for i in progressbar.progressbar(range(num_raw_data)):
			xs = np.random.uniform(-0.5,0.5,2)
			self.high_fid.set_initial_zero()
			self.high_fid.set_source_location(xs)
			self.high_fid.assemble_variational_problem()
			ts = self.high_fid.time_integration()
			raw_data.append(ts)

		raw_data = np.reshape(raw_data,[num_raw_data,raw_data[0].shape[0],-1])
		np.savez(path,raw_data=raw_data)

	def shuffle(self):
		self.torch_in.clear()
		self.torch_out.clear()

		rand_list = np.random.permutation( self.num_data )
		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				idx = rand_list[i*self.batch_size + j]
				local_in.append(self.raw_in[idx])
				local_out.append(self.raw_out[idx])
			local_in = np.reshape(local_in,[self.batch_size,-1])
			local_out = np.reshape(local_out,[self.batch_size,-1])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

	def shuffle_noise(self):
		self.torch_in.clear()
		self.torch_out.clear()

		rand_list = np.random.permutation( self.num_data )
		for i in range(self.num_batches):
			local_in = []
			local_out = []
			for j in range(self.batch_size):
				idx = rand_list[i*self.batch_size + j]
				noise = np.random.normal(0,0.003,self.raw_in[idx].shape)
				local_in.append(self.raw_in[idx] + noise)
				local_out.append(self.raw_out[idx])
			local_in = np.reshape(local_in,[self.batch_size,-1])
			local_out = np.reshape(local_out,[self.batch_size,-1])
			self.torch_in.append( torch.from_numpy( local_in ).float() )
			self.torch_out.append( torch.from_numpy( local_out ).float() )

def reshuffle_data(path_in,path_out):
	data_in = np.load(path_in)
	raw_data = data_in['raw_data']

	raw_in = []
	raw_out = []

	num_params = raw_data.shape[0]
	num_tspan = raw_data.shape[1]

	num_params = 100

	for i in range(num_params):
		print(i)
		for j in range(3,num_tspan):
				in_local =[]
				for k in range(-3,0):
					in_local.append( raw_data[i][j+k] )
				in_local = np.reshape(in_local,-1)
				out_local = raw_data[i][j]

				raw_in.append(in_local)
				raw_out.append(out_local)

	num_data = num_params*(num_tspan-3)
	raw_in = np.reshape(raw_in,[num_data,-1])
	raw_out = np.reshape(raw_out,[num_data,-1])

	np.savez(path_out,num_data=num_data,raw_in=raw_in,raw_out=raw_out)



if __name__ == '__main__':
	#d = data(num_batches=125,batch_size=4)
	#d.generate_raw(num_raw_data=100)
	#reshuffle_data(path_in='raw_data.npz',path_out='torch_data.npz')
	d = data(path='torch_data.npz',batch_size=128) 
	print(d.raw_in.shape)
	print(d.raw_out.shape)
